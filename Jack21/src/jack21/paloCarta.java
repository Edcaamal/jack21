/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jack21;

/**
 *
 * @author edgar
 */
public class paloCarta {


    // Código de colores para imprimir texto
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    /**
     * Método que genera valores alearorios limitandolos a un rango especificado
     * por el usuario
     *
     * @param rango Limite para generar valor aleatorio
     * @return un valor entero del 0 al rango proporcionado
     */    
    public static int randomize(int rango) {
        int valor = 0;
        // Genera un valor aletorio entre el rango entre 0.0 y 1.0
        // Se multiplica por un rango determinado por el usuario 

        double rnd = (Math.random() * rango);
        valor = (int) rnd;
        return valor;
    }
    
    /**
     * Método para generar el valor o la figura de la carta 
     * @param valor entero de la carta inicial
     * @return un valor o figura dependiendo el valor inicial enviado, 
     * TODO: Realizar las adecuaciones necesarias para que valor de la carta se 
     *       cambie si es una figura, segun las reglas J=10, Q=10 y K=10
     */
    
    public static String valorFiguraCarta(int valor) {
        String valorCarta;
        switch (valor) {
            case 1:
                valorCarta = ANSI_RED + "A" + ANSI_BLACK;
                break;
            case 11:
                valorCarta = ANSI_BLUE + "J" + ANSI_BLACK;
                break;
            case 12:
                valorCarta = ANSI_BLUE + "Q" + ANSI_BLACK;
                break;
            case 13:
                valorCarta = ANSI_BLUE + "K" + ANSI_BLACK;
                break;
            default:
                valorCarta = ANSI_BLACK + valor + "" + ANSI_BLACK;
                break;
        }        
        return valorCarta;
    }
    /**
     * Método para devolver el palo de la carta
     * @param valor entero aletorio generado para obtener un tipo de palo al que pertenecera la carta
     * @return tipo de palo de la carta
     */
    public static String paloCarta(int valor) {
        String figuraCarta;
        switch (valor) {
            case 1:
                figuraCarta = ANSI_PURPLE + "Espada " + ANSI_BLACK;
                break;
            case 2:
                figuraCarta = ANSI_PURPLE + "Corazón" + ANSI_BLACK;
                break;
            case 3:
                figuraCarta = ANSI_PURPLE + "Trebol " + ANSI_BLACK;
                break;
            case 4:
                figuraCarta = ANSI_PURPLE + "Rombo  " + ANSI_BLACK;
                break;
            default:
                figuraCarta = valor + "";
                break;
        }        
        return figuraCarta;
    }    
    

    /**
     * Método para imprimir en consola un modelo básico de carta
     * @param paloCarta   palo al que pertenece la carta amostrar
     * @param valorFigura valor o figur de la carta a mostrar
     */
    public static void imprimirCarta(String paloCarta, String valorFigura) {
       // System.out.println("12346789012345");
        //String.format   ("%-35s","NOMBRE") +" | ”);
    
        System.out.println("--------------");
        
       // System.out.println("|" );       
        System.out.println("|"+String.format   ("%-22s",valorFigura) +"|");       
        System.out.println("|"+String.format   ("%-12s"," ") +"|");        
        System.out.println("|    " + String.format   ("%-18s",paloCarta) +"|");
        System.out.println("|"+String.format   ("%-12s"," ") +"|");        
        System.out.println("|          "+String.format   ("%-12s",valorFigura) +"|");       
        System.out.println("--------------");       
        System.out.println("");
    }

    /**
     * Programa principal del Juego de 21
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int puntos = 0;
        int carta = 0;

        do {
            carta = randomize(12) + 1;
            puntos += carta;
            imprimirCarta(paloCarta(randomize(3) + 1), valorFiguraCarta(carta));
          
        } while (puntos <= 21);
        System.out.println(puntos);        
        

        
    }
    
}
    
