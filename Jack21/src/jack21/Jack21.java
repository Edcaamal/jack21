/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jack21;

/**
 *
 * @author edgar
 */
public class Jack21 {


    // Código de colores para imprimir texto
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    /**
     * Método que genera valores alearorios limitandolos a un rango especificado
     * por el usuario
     *
     * @param rango Limite para generar valor aleatorio
     * @return
     */    
    public static int randomize(int rango) {
        int valor = 0;
        // Genera un valor aletorio entre el rango entre 0.0 y 1.0
        // Se multiplica por un rango determinado por el usuario 

        double rnd = (Math.random() * rango);
        valor = (int) rnd;
        return valor;
    }
    
    public static String valorCarta(int valor) {
        String valorCarta;
        switch (valor) {
            case 1:
                valorCarta = ANSI_RED + "A" + ANSI_BLACK;
                break;
            case 11:
                valorCarta = ANSI_BLUE + "J" + ANSI_BLACK;
                break;
            case 12:
                valorCarta = ANSI_BLUE + "Q" + ANSI_BLACK;
                break;
            case 13:
                valorCarta = ANSI_BLUE + "K" + ANSI_BLACK;
                break;
            default:
                valorCarta = ANSI_BLACK + valor + "" + ANSI_BLACK;
                break;
        }        
        return valorCarta;
    }
    
    public static String paloCarta(int valor) {
        String figuraCarta;
        switch (valor) {
            case 1:
                figuraCarta = ANSI_PURPLE + "Espada " + ANSI_BLACK;
                break;
            case 2:
                figuraCarta = ANSI_PURPLE + "Corazón" + ANSI_BLACK;
                break;
            case 3:
                figuraCarta = ANSI_PURPLE + "Trebol " + ANSI_BLACK;
                break;
            case 4:
                figuraCarta = ANSI_PURPLE + "Rombo  " + ANSI_BLACK;
                break;
            default:
                figuraCarta = valor + "";
                break;
        }        
        return figuraCarta;
    }    
    
    public static void imprimirCarta(String figura, String valor) {
       // System.out.println("12346789012345");
        //String.format   ("%-35s","NOMBRE") +" | ”);
    
        System.out.println("--------------");
        
       // System.out.println("|" );       
        System.out.println("|"+String.format   ("%-22s",valor) +"|");       
        System.out.println("|"+String.format   ("%-12s"," ") +"|");        
        System.out.println("|    " + String.format   ("%-18s",figura) +"|");
        System.out.println("|"+String.format   ("%-12s"," ") +"|");        
        System.out.println("|          "+String.format   ("%-12s",valor) +"|");       
        System.out.println("--------------");       
        System.out.println("");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int puntos = 0;
        int carta = 0;
        /*
        for (int i =1; i<= 13; i++){
            System.out.print(figuraCarta(randomize(3)+1)+"-");
            System.out.println(valorCarta(randomize(12)+1));
        }
        System.out.println("-----------------------------");
         */
        do {
            carta = randomize(12) + 1;
            puntos += carta;
            imprimirCarta(paloCarta(randomize(3) + 1), valorCarta(carta));
            //System.out.print(figuraCarta(randomize(3)+1)+"-");
            //System.out.println(valorCarta(carta));              
        } while (puntos <= 21);
        System.out.println(puntos);        
        

        
    }
    
}
    
